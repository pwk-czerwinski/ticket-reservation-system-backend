<?php
/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 18.11.17
 * Time: 18:26
 */

namespace ReservationSystem;

use JsonSerializable;

class Event implements JsonSerializable
{
  private $id;
  private $name;
  private $dateOfEvent;

  public function __construct(int $id, string $name, string $dateOfEvent)
  {
    $this->id = $id;
    $this->name = $name;
    $this->dateOfEvent = $dateOfEvent;
  }

  public function getId(): int {
    return $this->id;
  }

  public function getName(): string {
    return $this->name;
  }

  public function getDateOfEvent(): string {
    return $this->dateOfEvent;
  }

  public function jsonSerialize()
  {
    $currentSession = SessionLifecycle::getInstance();
    $currentSession->sessionStart();

    if (isset($_SESSION['event']['id'])) {
      $this->setId($_SESSION['event']['id']);
    }

    return [
      'event' => [
        'id' => $this->getId(),
        'name' => $this->getName(),
        'dateOfEvent' => $this->getDateOfEvent()
      ]
    ];
  }

  private function setId(int $id) {
    $this->id = $id;
  }
}
