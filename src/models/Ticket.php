<?php
/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 31.08.17
 * Time: 21:20
 */

namespace ReservationSystem;

use JsonSerializable;

class Ticket implements JsonSerializable
{
    /**
     * @var TicketType
     */
    private $type;
    /**
     * @var StadiumPlace
     */
    private $place;
    /**
     * @var float
     */
    private $price;

    public function __construct(string $type, StadiumPlace $place, float $price)
    {
        $this->type = $type;
        $this->place = $place;
        $this->price = $price;
    }

    public function jsonSerialize()
    {
        return [
            'ticket' => [
                'type' => $this->type,
                'stadiumplace' => [
                    'sector' => $this->place->getSector(),
                    'row' => $this->place->getRow(),
                    'place' => $this->place->getPlace()
                ],
                'price' => $this->price
            ]
        ];
    }
}