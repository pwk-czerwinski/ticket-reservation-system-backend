<?php
/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 16.09.17
 * Time: 20:09
 */

namespace ReservationSystem;

use JsonSerializable;

class Customer implements JsonSerializable
{
  private $firstName;
  private $lastName;
  private $email;

  public function __construct(string $firstName, string $lastName, string $email) {
    $this->firstName = $firstName;
    $this->lastName = $lastName;
    $this->email = $email;
  }

  public function getFirstName(): string {
    return $this->firstName;
  }

  public function getLastName(): string {
    return $this->lastName;
  }

  public function getEmail(): string {
    return $this->email;
  }

  public function jsonSerialize() {
    return [
      'personal_data' => [
        'firstName' => $this->getFirstName(),
        'lastName' => $this->getLastName(),
        'email' => $this->getEmail()
      ]
    ];
  }

  private function setFirstName(string $firstName) {
    $this->firstName = $firstName;
  }

  private function setLastName(string $lastName) {
    $this->lastName = $lastName;
  }

  private function setEmail(string $email) {
    $this->email = $email;
  }
}
