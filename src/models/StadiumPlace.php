<?php
/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 16.09.17
 * Time: 19:48
 */

namespace ReservationSystem;

use JsonSerializable;

class StadiumPlace implements JsonSerializable
{
  private $sector;
  private $row;
  private $place;

  public function __construct(string $sector, int $row, int $place) {
    $this->sector = $sector;
    $this->row = $row;
    $this->place = $place;
  }

  public function getSector(): string {
    return $this->sector;
  }

  public function getRow(): int {
    return $this->row;
  }

  public function getPlace(): int {
    return $this->place;
  }

  public function jsonSerialize() {
    return [
      'stadium_place' => ''
    ];
  }

  private function setSector($sector) {
    $this->sector = $sector;
  }

  private function setRow($row) {
    $this->row = $row;
  }

  private function setPlace($place) {
    $this->place = $place;
  }
}
