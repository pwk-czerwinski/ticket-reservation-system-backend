<?php
/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 01.12.17
 * Time: 20:23
 */

namespace ReservationSystem;


class SessionLifecycle
{
  private static $instance;

  private $event;
  private $sector;
  private $places;
  private $customer;

  private function __construct() {}
  private function __clone() {}

  public static function getInstance()
  {
    if (self::$instance === null)
    {
      self::$instance = new SessionLifecycle();
    }
    return self::$instance;
  }

  public function sessionStart() {
      session_start();
  }

  public function sessionDestroy() {
    if (isset($_COOKIE['PHPSESSID'])) {
      if (!isset($_SESSION)) {
        session_start();
      }
      session_unset();
      session_destroy();
      setcookie("PHPSESSID", "", time() - 3600);
    }
  }

  public function deleteVariable(string $variableName) {
    if (isset($_COOKIE['PHPSESSID'])) {
      if (isset($_SESSION[$variableName])) {
        unset($_SESSION[$variableName]);
      }
    }
  }

  public function chooseEvent(Event $event) {
    $this->setEvent($event);
    $jsonEvent = json_encode($this->event);
    $_SESSION['event'] = $jsonEvent;
  }

  public function chooseSector(string $sector) {
    $this->sessionStart();
    $this->setSector($sector);
    $_SESSION['sector'] = $sector;
  }

  public function choosePlaces(array $places) {
    $this->sessionStart();
    $this->setPlaces($places);
    $_SESSION['places'] = $places;
  }

  public function addCustomer(Customer $customer) {
    $this->sessionStart();
    $this->setCustomer($customer);
    $jsonCustomer = json_encode($this->customer);
    $_SESSION['personal_data'] = $jsonCustomer;
  }

  public function getReservationData() {
    $this->sessionStart();
    $reservationData = $_SESSION;

    return $reservationData;
  }

  /**
   * Getters and setters
   */
  public function getEvent() {
    $eventData = $_SESSION['event'];
    $eventData = json_decode($eventData, true);
    $eventId = $eventData['event']['id'];
    $eventName = $eventData['event']['name'];
    $dateOfEvent = $eventData['event']['dateOfEvent'];
    $event = new Event($eventId, $eventName, $dateOfEvent);
    $this->setEvent($event);

    return $this->event;
  }

  public function getSector() {
    $sector = $_SESSION['sector'];
    $this->setSector($sector);

    return $this->sector;
  }

  public function getCustomer() {
    $customerData = $_SESSION['personal_data'];
    $customerData = json_decode($customerData, true);
    $customerFirstName = $customerData['personal_data']['firstName'];
    $customerLastName = $customerData['personal_data']['lastName'];
    $customerEmail = $customerData['personal_data']['email'];
    $customer = new Customer($customerFirstName, $customerLastName, $customerEmail);
    $this->setCustomer($customer);

    return $this->customer;
  }

  public function getPlaces() {
    $places = $_SESSION['places'];
    $this->setPlaces($places);

    return $this->places;
  }

  private function setEvent(Event $event) {
    $this->event = $event;
  }

  private function setSector(string $sector) {
    $this->sector = $sector;
  }

  private function setCustomer(Customer $customer) {
    $this->customer = $customer;
  }

  private function setPlaces($places) {
    $this->places = $places;
  }
}
