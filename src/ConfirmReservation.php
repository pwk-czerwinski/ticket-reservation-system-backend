<?php
/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 18.11.17
 * Time: 18:22
 */

namespace ReservationSystem;

use PDOClient;

class ConfirmReservation
{
  private $event;
  private $stadiumPlaces;
  private $customer;
  private $DBConnection;

  public function __construct()
  {
    $currentSession = SessionLifecycle::getInstance();
    $currentSession->sessionStart();
    $event = $currentSession->getEvent();
    $this->event = $event;
    $sector = $currentSession->getSector();
    $stadiumPlaces = $currentSession->getPlaces();

    foreach ($stadiumPlaces as $stadiumPlace) {
      $row = (int) $stadiumPlace['row'];
      $place = (int) $stadiumPlace['place'];
      $this->stadiumPlaces[] = new StadiumPlace($sector, $row, $place);
    }
    $customer = $currentSession->getCustomer();
    $this->customer = $customer;

    $this->DBConnection = PDOClient::getInstance();
  }

  public function finish() {
    $customerId = $this->addCustomerToDB();
    $reservationId = -1;
    $stadiumPlacesIds = [];

    if ($customerId !== -1) {
      $reservationId = $this->createReservation($customerId);
    }

    if ($reservationId !== -1) {
      $stadiumPlacesIds = $this->findStadiumPlaceIds();
    }

    if (sizeof($stadiumPlacesIds) > 0) {
      $this->finishReservation($stadiumPlacesIds, $reservationId);
    }

    $message = 'Pomyślnie zarezerwowano biety, które zostały wysłane na adres email podany w formularzu.' .
      PHP_EOL . 'Prosimy sprawdzić skrzynkę pocztową.';

    return $message;
  }

  private function findStadiumPlaceIds(): array {
    $placesIds = [];

    foreach ($this->stadiumPlaces as $stadiumPlace) {
      $sectorName = $stadiumPlace->getSector();
      $row = $stadiumPlace->getRow();
      $place = $stadiumPlace->getPlace();
      $result = $this->DBConnection->prepare(
        "SELECT stadium_places.id FROM stadium_places JOIN stadium_sectors ON stadium_sectors.id = stadium_places.id_sector
                  WHERE (stadium_sectors.name = :sector_name) AND (row = :row) AND (place = :place)"
      );
      $result->bindParam(":sector_name", $sectorName);
      $result->bindParam(":row", $row);
      $result->bindParam(":place", $place);
      $result->execute();

      if ($result) {
        $outputData = $result->fetchAll();
        $placesIds[] = $outputData[0]['id'];
      }
    }
    return $placesIds;
  }

  private function createReservation($customerId): int {
    $result = $this->DBConnection->prepare(
      "INSERT INTO reservations (created, id_customer)
                VALUES (NOW(), :idCustomer) ");
    $result->bindParam(":idCustomer", $customerId);
    $result->execute();

    return $this->DBConnection->lastInsertId();
  }

  private function finishReservation($stadiumPlacesIds, $idReservation) {
    $currentSession = SessionLifecycle::getInstance();
    $event = $currentSession->getEvent();
    $eventId = $event->getId();

    for ($i = 0; $i < sizeof($stadiumPlacesIds); $i++) {
      $result = $this->DBConnection->prepare(
        "UPDATE tickets SET id_reservation = :idReservation
                  WHERE (id_stadium_place = :idStadiumPlace) AND (id_event = :idEvent)"
      );
      $result->bindParam(":idReservation", $idReservation);
      $result->bindParam(":idStadiumPlace", $stadiumPlacesIds[$i]);
      $result->bindParam(":idEvent", $eventId);
      $result->execute();
    }
  }

  private function addCustomerToDB(): int {
    $email = $this->customer->getEmail();
    $firstName = $this->customer->getFirstName();
    $lastName = $this->customer->getLastName();
    $customerId = $this->getCustomerId();

    if ($customerId === -1) {
      $result = $this->DBConnection->prepare(
        "INSERT INTO customers (email, first_name, last_name)
                VALUES (:email, :firstName, :lastName) ");
      $result->bindParam(":email", $email);
      $result->bindParam(":firstName", $firstName);
      $result->bindParam(":lastName", $lastName);
      $result->execute();
      $customerId = $this->getCustomerId();
    } else {
      return -1;
    }
    return $customerId;
  }

  private function getCustomerId(): int {
    $email = $this->customer->getEmail();
    $result = $this->DBConnection->prepare(
      "SELECT id FROM customers WHERE email = :email"
    );
    $result->bindParam(":email", $email);
    $result->execute();

    if ($result) {
      $outputData = $result->fetchAll();
      if (isset($outputData[0])) {
        return $outputData[0]['id'];
      } else {
        return -1;
      }
    }
  }
}
