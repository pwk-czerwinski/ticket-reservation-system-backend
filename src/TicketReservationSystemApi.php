<?php
/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 16.09.17
 * Time: 20:25
 */

namespace ReservationSystem;

use PDOClient;

require __DIR__.'/PDOClient.php';
require __DIR__ . '/models/Event.php';
require __DIR__ . '/models/StadiumPlace.php';
require __DIR__ . '/models/Customer.php';
require __DIR__.'/ConfirmReservation.php';
require __DIR__.'/SessionLifecycle.php';
require __DIR__.'/Session.php';

class TicketReservationSystemApi
{
  public static function getEvents()
  {
    if(isset($_COOKIE['PHPSESSID'])) {
      $currentSession = SessionLifecycle::getInstance();
      $currentSession->sessionDestroy();
    }

    $conn = PDOClient::getInstance();
    $result = $conn->prepare("
      SELECT DISTINCT events.id, events.name, events.date_of_event, events.image_url FROM events
      JOIN tickets ON events.id = tickets.id_event 
    ");
    $result->execute();

    if ($result) {
      $outputData = $result->fetchAll();
    }
    $events = [];

    foreach ($outputData as $row) {
      $events[] = array(
        'id' => $row['id'],
        'name' => $row['name'],
        'date_of_event' => $row['date_of_event'],
        'image_url' => $row['image_url']
      );
    }

    $events = array('events' => $events);

    return [
      'body' => $events,
      'status' => 200
    ];
  }

  public static function getSectors($json)
  {
    $json = json_decode($json, true);
    $eventId = $json['eventId'];

    if (isset($_SESSION['sector'])) {
      $currentSession = SessionLifecycle::getInstance();
      $currentSession->deleteVariable('sector');
    }

    $conn = PDOClient::getInstance();
    $result = $conn->prepare("
      SELECT DISTINCT name, number_of_rows, number_of_places, price, COUNT(name) AS number_of_free_places 
      FROM stadium_sectors
      JOIN stadium_places ON stadium_sectors.id = stadium_places.id_sector
      JOIN tickets ON tickets.id_stadium_place = stadium_places.id
      WHERE id_event = :id_event
      AND tickets.id_reservation IS NULL 
      GROUP BY name
    ");
    $result->bindParam(":id_event", $eventId);
    $result->execute();

    if ($result) {
      $outputData = $result->fetchAll();
    }
    $sectors = [];

    foreach ($outputData as $row) {
      $sectors[] = array(
        'name' => $row['name'],
        'number_of_free_places' => $row['number_of_free_places'],
        'price' => $row['price']
      );
    }

    $sectors = array('sectors' => $sectors);

    return [
      'body' => $sectors,
      'status' => 200
    ];
  }

  public static function getSectorPlaces($json)
  {
    $currentSession = SessionLifecycle::getInstance();
    $currentSession->sessionStart();

    $eventId = '';
    $sector = '';
    $json = json_decode($json, true);

    if (isset($_SESSION['event']) && isset($_SESSION['sector'])) {
      $event = json_decode($_SESSION['event'], true);
      $eventId = $event['event']['id'];
      $sector = $_SESSION['sector'];
    } elseif (!empty($json['idEvent']) && !empty($json['sectorName'])) {
      $eventId = $json['idEvent'];
      $sector = $json['sectorName'];
    }

    $conn = PDOClient::getInstance();
    $result = $conn->prepare("SELECT row, place, id_reservation, price FROM stadium_places
                      JOIN stadium_sectors ON stadium_sectors.id = stadium_places.id_sector
                      JOIN tickets ON tickets.id_stadium_place = stadium_places.id
                      WHERE stadium_sectors.name = :sector_name AND id_event = :id_event");
    $result->bindParam(":sector_name", $sector);
    $result->bindParam(":id_event", $eventId);
    $result->execute();

    $outputData = [];
    if ($result) {
      $outputData = $result->fetchAll();
    }

    $sectorPlaces = [];
    $places = [];
    $currentRow = '';

    foreach ($outputData as $row) {
      $currentPlace = array(
        'place' => $row['place'],
        'reserved' => $row['id_reservation'],
        'price' => $row['price']
      );

      if ($currentRow !== '') {
        if ($currentRow !== $row['row']) {
          $rowPlaces = array('row' => $currentRow, 'places' => $places);
          $sectorPlaces[] = $rowPlaces;
          $places = [];
          $places[] = $currentPlace;
          $currentRow = $row['row'];
        } else {
          $places[] = $currentPlace;
        }
      } else {
        $places[] = $currentPlace;
        $currentRow = $row['row'];
      }
    }

    $rowPlaces = array('row' => $currentRow, 'places' => $places);
    $sectorPlaces[] = $rowPlaces;

    $sectorPlaces = array('sector_name' => $sector, 'stadium_places' => $sectorPlaces);

    return [
      'body' => $sectorPlaces,
      'status' => 200
    ];
  }

  public static function chooseEvent($json)
  {
    $bodyContent = '';
    $status = 200;
    $json = json_decode($json, true);

    if (!empty($json) && isset($json['choosedEvent']['id'])) {
      $currentSession = SessionLifecycle::getInstance();

      if (!isset($_SESSION['event'])) {
        $eventId = $json['choosedEvent']['id'];
        $eventName = $json['choosedEvent']['name'];
        $dateOfEvent = $json['choosedEvent']['dateOfEvent'];
        $event = new Event($eventId, $eventName, $dateOfEvent);
        $currentSession->chooseEvent($event);
      }

      if (isset($_SESSION['sector'])) {
        $currentSession->deleteVariable('sector');
      }
    } else {
      $bodyContent = 'EMPTY_EVENT';
      $status = 204;
    }

    return [
      'body' => $bodyContent,
      'status' => $status
    ];
  }

  public static function chooseSector($json)
  {
    $bodyContent = '';
    $status = 200;

    if (!empty($json)) {
      $json = json_decode($json, true);
      $sector = $json['sector'];
      $currentSession = SessionLifecycle::getInstance();
      $currentSession->chooseSector($sector);
    } else {
      $bodyContent = 'EMPTY_SECTOR';
      $status = 204;
    }

    return [
      'body' => $bodyContent,
      'status' => $status
    ];
  }

  public static function choosePlaces($json)
  {
    $bodyContent = '';
    $status = 200;

    if (!empty($json)) {
      $json = json_decode($json, true);
      $stadiumPlaces = [];
      foreach ($json['places'] as $place) {
        $stadiumPlaces[] = ['row' => $place['row'], 'place' => $place['place']];
      }

      $currentSession = SessionLifecycle::getInstance();
      $currentSession->choosePlaces($stadiumPlaces);
    }

    return [
      'body' => $bodyContent,
      'status' => $status
    ];
  }

  public static function addPersonalData($json)
  {
    $bodyContent = '';
    $status = 200;

    if (!empty($json)) {
      $json = json_decode($json, true);
      $firstName = $json['personalData']['firstName'];
      $lastName = $json['personalData']['lastName'];
      $email = $json['personalData']['email'];
      $customer = new Customer($firstName, $lastName, $email);
      $currentSession = SessionLifecycle::getInstance();
      $currentSession->addCustomer($customer);
    }

    return [
      'body' => $bodyContent,
      'status' => $status
    ];
  }

  public static function getReservationData()
  {
    $currentSession = SessionLifecycle::getInstance();
    $reservationData = $currentSession->getReservationData();

    return [
      'body' => $reservationData,
      'status' => 200
    ];
  }

  public static function confirmReservation()
  {
    $confirmReservation = new ConfirmReservation();
    $message = $confirmReservation->finish();

    if ($message !== '') {
      $currentSession = SessionLifecycle::getInstance();
      $currentSession->sessionDestroy();
    }

    return [
      'body' => $message,
      'status' => 200
    ];
  }
}
