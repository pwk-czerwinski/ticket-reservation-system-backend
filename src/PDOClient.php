<?php
/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 28.09.17
 * Time: 21:49
 */

class PDOClient
{
  private static $instance;

  private function __construct() {}
  private function __clone() {}

  public static function getInstance()
  {
    if (self::$instance === null)
    {
      $servername = "localhost";
      $username = "root";
      $password = "Work2017!";
      $dbname = "ticket_reservation_system_test";

      self::$instance = new PDO("mysql:host=$servername;dbname=$dbname;charset=utf8", $username, $password);
      self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    return self::$instance;
  }
}
