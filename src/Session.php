<?php
/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 20.11.17
 * Time: 21:33
 */

namespace ReservationSystem;

use PDOClient;

class Session
{
  private $DBConnection;

  public function __construct()
  {
    $this->DBConnection = PDOClient::getInstance();

    session_set_save_handler(
      array($this, "open"),
      array($this, "close"),
      array($this, "read"),
      array($this, "write"),
      array($this, "destroy"),
      array($this, "gc")
    );

    register_shutdown_function('session_write_close');
  }

  public function open()
  {
    if ($this->DBConnection) {
      return true;
    }
    return false;
  }

  public function close()
  {
    $this->DBConnection = null;
    return true;
  }

  public function read($id)
  {
    $result = $this->DBConnection->prepare("SELECT data FROM sessions WHERE id = :id");
    $result->bindParam(":id", $id);
    $result->execute();

    if ($result) {
      $row = $result->fetchAll();

      if (isset($row[0])) {
        return $row[0]['data'];
      } else {
        return '';
      }
    }
  }

  public function write($id, $data)
  {
    $access = time();
    $result = $this->DBConnection->prepare("REPLACE INTO sessions VALUES (:id, :access, :data)");
    $result->bindParam(":id", $id);
    $result->bindParam(":access", $access);
    $result->bindParam(":data", $data);
    $result->execute();

    if ($result) {
      return true;
    }
    return false;
  }

  public function destroy($id)
  {
    $result = $this->DBConnection->prepare("DELETE FROM sessions WHERE id = :id");
    $result->bindParam(":id", $id);
    $result->execute();

    if ($result) {
      return true;
    }
    return false;
  }

  public function gc($max)
  {
    $old = time() - $max;
    $result = $this->DBConnection->prepare("DELETE * FROM sessions WHERE access < :old");
    $result->bindParam(":old", $old);
    $result->execute();

    if ($result) {
      return true;
    }
    return false;
  }
}

new Session();
