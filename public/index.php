<?php
/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 31.08.17
 * Time: 21:07
 */

require_once '../vendor/autoload.php';
require __DIR__.'/../src/TicketReservationSystemApi.php';

use ReservationSystem\TicketReservationSystemApi;
use Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;

$app = new Silex\Application();
$app['debug'] = true;

$endpoints = [
  'events' => '/events',
  'sectors' => '/sectors',
  'sector-places' => '/sector-places',
  'choose-event' => '/choose-event',
  'choose-sector' => '/choose-sector',
  'choose-places' => '/choose-places',
  'add-personal-data' => '/add-personal-data',
  'reservation-data' => '/reservation-data',
  'confirm-reservation' => '/confirm-reservation'
];

$app->get($endpoints['events'], function () {
  $result = TicketReservationSystemApi::getEvents();
  $response = new Response(json_encode($result['body']), $result['status']);

  return $response;
});

$app->post($endpoints['sectors'], function (Request $request) {
  $requestContent = $request->getContent();
  $result = TicketReservationSystemApi::getSectors($requestContent);
  $response = new Response(json_encode($result['body']), $result['status']);

  return $response;
});

$app->post($endpoints['sector-places'], function (Request $request) {
  $requestContent = $request->getContent();
  $result = TicketReservationSystemApi::getSectorPlaces($requestContent);
  $response = new Response(json_encode($result['body']), $result['status']);

  return $response;
});

$app->post($endpoints['choose-event'], function (Request $request) {
  $result = TicketReservationSystemApi::chooseEvent($request->getContent());
  $response = new Response(json_encode($result['body'], $result['status']));

  return $response;
});

$app->post($endpoints['choose-sector'], function (Request $request) {
  $result = TicketReservationSystemApi::chooseSector($request->getContent());
  $response = new Response(json_encode($result['body'], $result['status']));

  return $response;
});

$app->post($endpoints['choose-places'], function (Request $request) {
  $result = TicketReservationSystemApi::choosePlaces($request->getContent());
  $response = new Response(json_encode($result['body'], $result['status']));

  return $response;
});

$app->post($endpoints['add-personal-data'], function (Request $request) {
  $result = TicketReservationSystemApi::addPersonalData($request->getContent());
  $response = new Response(json_encode($result['body'], $result['status']));

  return $response;
});

$app->get($endpoints['reservation-data'], function () {
  $result = TicketReservationSystemApi::getReservationData();
  $response = new Response(json_encode($result['body']), $result['status']);

  return $response;
});

$app->get($endpoints['confirm-reservation'], function () {
  $result = TicketReservationSystemApi::confirmReservation();
  $response = new Response(json_encode($result['body']), $result['status']);

  return $response;
});

foreach ($endpoints as $endpoint) {
  $app->options($endpoint, function () {
    $response = new Response('', 200);
    $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
    $response->headers->set('Access-Control-Allow-Headers', 'Content-Type,x-requested-with');

    return $response;
  });
}

$app->after(function (Request $request, Response $response) {
  $response->headers->set('Access-Control-Allow-Origin', $request->server->get('HTTP_ORIGIN'));
  $response->headers->set('Access-Control-Allow-Credentials', 'true');
  $response->headers->set('Content-Type', 'application/json');
});

$app->run();
